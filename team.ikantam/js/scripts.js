function bgImage($data_bg, $data_image){
    var _bg     = document.querySelectorAll($data_bg); 
    for (var i = 0; i < _bg.length; i++) {
        var _img    = _bg[i].querySelector($data_image);
        _bg[i].style.backgroundImage = 'url(' + _img.src + ')';
        _bg[i].removeChild(_img);
    }   
}
document.addEventListener('DOMContentLoaded', function() {
	bgImage('[data-bg]', '[data-img]');
})